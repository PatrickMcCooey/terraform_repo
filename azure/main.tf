resource "azurerm_resource_group" "qse-search" {
  name     = "qse-search-resources"
  location = "West Europe"
}

resource "azurerm_kubernetes_cluster" "qse-search" {
  name                = "qse-search-aks1"
  location            = azurerm_resource_group.qse-search.location
  resource_group_name = azurerm_resource_group.qse-search.name
  dns_prefix          = "qse-searchaks1"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  service_principal {
    client_id     = "00000000-0000-0000-0000-000000000000"
    client_secret = "00000000000000000000000000000000"
  }

  tags = {
    Environment = "Production"
  }
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.qse-search.kube_config.0.client_certificate
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.qse-search.kube_config_raw
}
